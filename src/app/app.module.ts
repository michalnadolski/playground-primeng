import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { CardModule } from 'primeng/card';
import { CardsPlaygroundComponent } from './cards-playground/cards-playground.component';
import { ButtonsPlaygroundComponent } from './buttons-playground/buttons-playground.component';
import { DropdownsPlaygroundComponent } from './dropdowns-playground/dropdowns-playground.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ContentProjectionComponent } from './content-projection/content-projection.component';
import { LayoutServiceComponent } from './layout-service/layout-service.component';
import { HIGHLIGHT_OPTIONS } from 'ngx-highlightjs';
import { HighlightModule } from 'ngx-highlightjs';


@NgModule({
  declarations: [
    AppComponent,
    CardsPlaygroundComponent,
    ButtonsPlaygroundComponent,
    DropdownsPlaygroundComponent,
    PageNotFoundComponent,
    WelcomeComponent,
    ContentProjectionComponent,
    LayoutServiceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ButtonModule,
    RippleModule,
    CardModule,
    HighlightModule
  ],
  providers: [{
    provide: HIGHLIGHT_OPTIONS,
    // useValue: {
    //   fullLibraryLoader: () => import('highlight.js')
    // }
    useValue: {
      coreLibraryLoader: () => import('highlight.js/lib/core'),
      lineNumbersLoader: () => import('ngx-highlightjs/line-numbers'), // Optional, only if you want the line numbers
      languages: {
        typescript: () => import('highlight.js/lib/languages/typescript'),
        xml: () => import('highlight.js/lib/languages/xml'),
        css: () => import('highlight.js/lib/languages/css'),
        json: () => import('highlight.js/lib/languages/json')
      },
      themePath: 'highlight.js/styles/atom-one-dark.min.css' // Optional, and useful if you want to change the theme dynamically
    }
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
