import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardsPlaygroundComponent } from './cards-playground.component';

describe('CardsPlaygroundComponent', () => {
  let component: CardsPlaygroundComponent;
  let fixture: ComponentFixture<CardsPlaygroundComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CardsPlaygroundComponent]
    });
    fixture = TestBed.createComponent(CardsPlaygroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
