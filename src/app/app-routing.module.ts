import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { CardsPlaygroundComponent } from './cards-playground/cards-playground.component';
import { DropdownsPlaygroundComponent } from './dropdowns-playground/dropdowns-playground.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ButtonsPlaygroundComponent } from './buttons-playground/buttons-playground.component';
import { ContentProjectionComponent } from './content-projection/content-projection.component';

const routes: Routes = [
  { path: 'welcome', component: WelcomeComponent },
  { path: 'card', component: CardsPlaygroundComponent },
  { path: 'button', component: ButtonsPlaygroundComponent },
  { path: 'dropdown', component: DropdownsPlaygroundComponent },
  { path: 'content-projection', component: ContentProjectionComponent },
  { path: '',   redirectTo: '/welcome', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
