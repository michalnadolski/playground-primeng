import { AfterViewInit, Component, OnInit } from '@angular/core';
import { NavigationEnd, Params, Router, Routes } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { PrimeNGConfig } from 'primeng/api';
import { CardsPlaygroundComponent } from './cards-playground/cards-playground.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { DropdownsPlaygroundComponent } from './dropdowns-playground/dropdowns-playground.component';
import { WelcomeComponent } from './welcome/welcome.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'playground-primeng';
  url = '';
  code = `<pre>
  <code [highlight]="code" [languages]="['typescript']" (highlighted)="onHighlight($event)" [lineNumbers]="true">
  </code>
</pre>
  `;
  
  constructor(
    private primengConfig: PrimeNGConfig,
    private router: Router) {}

  ngAfterViewInit(): void {
    this.router.events.subscribe((navigationEnd) => {
      if (navigationEnd instanceof NavigationEnd) {
        this.url = navigationEnd.url;
      }
    });
  }

  ngOnInit() {
    this.primengConfig.ripple = true;
    
  }
  public onHighlight($event: unknown): void {

    console.log('onHighlight, event: ', $event);
  }
}
