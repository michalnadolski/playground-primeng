import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownsPlaygroundComponent } from './dropdowns-playground.component';

describe('DropdownsPlaygroundComponent', () => {
  let component: DropdownsPlaygroundComponent;
  let fixture: ComponentFixture<DropdownsPlaygroundComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DropdownsPlaygroundComponent]
    });
    fixture = TestBed.createComponent(DropdownsPlaygroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
